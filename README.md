# Bash Scripting, SSH and SCP

This class covers topics regarding bash scripting, `SSH` and `SCP`


#### Bash Scripting
Bash scripting is the ability to declarity type bash command that provision a machine on a file, and then run it against a machine.

<u>Provisioning a machine includes:</u>

- making files and directories
- editing / configuring files
- installing software
- starting and stopping files
- creating intitial files
- sending files and code over to the computer (scp)


#### SSH

SSH or Secure Shell - is very useful to securely log into a computer at a distance.

It allows us to open a terminal on said computer with shell.
We can then use all our bash knowledge to configure the machine. 

Main commands:

```bash
# remote loggin to a machine 
#syntax $ ssh <option> <user>@machine.ip

#Example
$ ssh -i ~/.ssh/mykey.pem ubuntu@34.21.23.4
or 
$ ssh -i ~/.ssh/mykey.pem thor@34.21.23.4

```

SSH also allows you to run commands remotely. 

```bash
# ls in a remote machine
$ $ ssh -i ~/.ssh/mykey.pem ubuntu@34.21.23.4

# you can just add the command after the ssh user and machine

$ ssh -i ~/.ssh/mykey.pem ubuntu@34.245.2.98 ls demos
$ ssh -i ~/.ssh/mykey.pem ubuntu@34.245.2.98 cat bad.txt

## Create files in the machine

$ ssh -i ~/.ssh/mykey.pem ubuntu@34.245.2.98 touch nicole.md
```

#### Secure Copy into remote location (SCP)

Imagine like 'mv' but with ssh keys and remote computers

Used to move files and/or folders into remote machines. This is useful to move .sh files or folers with code that need to be setup.

```bash
# Syntax

# scp -i ~/.ssh/ch9_shared.pem <source/file> <target>
# scp -i ~/.ssh/ch9_shared.pem <source/file> <user>@<ip>:<path/to/location>

$ scp -i ~/.ssh/ch9_shared.pem bash_script101.sh ubuntu@34.21.23.4:/users/ubuntu/nicole/
$ scp -i ~/.ssh/ch9_shared.pem bash_script101.sh ubuntu@34.245.2.98:/home/ubuntu/

You have to run the code under the start: bashcripting_ssh_scp not under the ubuntu otherwise it will not recognise it. 



# Syntax to send folders
$ scp -i ~/.ssh/ch9_shared.pem -r nicole_website ubuntu@34.245.2.98:/home/ubuntu/


```